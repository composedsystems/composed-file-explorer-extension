##[2.0.1] - 2020-04-06
- Built against MVA-Viewable G Package v0.1.1

##[2.0.0] - 2020-03-26
- Built against MVA-Viewable G Package

##[1.1.4] - 2019-10-29
- Updated Handle Click on Tree Item to pass in Button for differentiating between right and left clicks

##[1.1.3] - 2019-10-29
- Added accessor for setting selection color
- Updated Handle Click on Tree Item to pass in Plat Mods for differentiating between right and left clicks

##[1.1.2] - 2019-10-08
- Refactoring to speed up tree redraw

##[1.1.1] - 2019-09-16
- Added ability for open/closed tree items to maintain open state on tree redraw

##[1.1.0] - 2019-09-16
- Updated to MVA v2.2.3

##[1.0.10] - 2019-08-10
- Updated how the filtering class interacts with the Tree Explorer

##[1.0.9] - 2019-08-08
- Added ability to Register for a redraw of the tree control as a notification message
- This update includes a new Must Override Method "Register for Draw Tree.vi"

##[1.0.8] - 2019-08-08
- Updated the Receive Shortcut Menu Selection Event.vi to take the Coordinates as an input
- Cosmetic updates to other project items
- Re-linked to MVA v2.1.3, there was an issue with the Variant Extension dependency for some reason

##[1.0.7] - 2019-08-08
- Updated to MVA v2.1.3

##[1.0.6] - 2019-08-07
- Added icon to Test File Explorer class
- Replaced property node accessor for Tree ref with a sub.vi accessor
- Added Chris Cilino as author

##[1.0.5] - 2019-08-07
- Adding ability to customize tree right click menus and responses to right click menu options.
- Added unit tests and some manual ui tests.

##[1.0.4] - 2019-08-05
- Adding test suite and initial test cases

##[1.0.3] - 2019-08-05
- Building before a pull request

##[1.0.2] - 2019-08-02
- Fixes [Issue #4](https://bitbucket.org/composedsystems/composed-file-explorer-extension/issues/4/testappassemblervi-should-not-be-in-the)
- Changed scoping of static methods from private to public

##[1.0.1] - 2019-08-02
- Fixes [Issue #3](https://bitbucket.org/composedsystems/composed-file-explorer-extension/issues/3/we-need-an-accessor-for-folder-exclusion)
- Removed Beta Tag
- Updated Readme

##[1.0.0] - 2019-08-01
- First official release

##[0.0.2] - 2019-08-01
- Built up a test application for exercising the view
- Starting to implement UX features

##[0.0.1] - 2019-07-31
- Initial commit